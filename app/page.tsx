import { Link } from "@nextui-org/link";
import { Snippet } from "@nextui-org/snippet";
import { Code } from "@nextui-org/code";
import { button as buttonStyles } from "@nextui-org/theme";
import { Divider } from "@nextui-org/divider";
import { siteConfig } from "@/config/site";
import { title, subtitle } from "@/components/primitives";
import { GithubIcon } from "@/components/icons";

export default function Home() {
  return (
    <section className="h-100% flex flex-col gap-4 py-8 md:py-10">
      <div className="inline-block max-w-lg">
        <h1 className={title({ color: "blue", size: "xxl" })}>
          Simplicity&nbsp;
        </h1>
        <br />
        <h1 className={title({ size: "xl" })}>Over Complexity</h1>
        <div>
          <Divider className="my-7" />
          <h2 className={subtitle({})}>
            At My Calendar, we prioritize simplicity in managing your time. Our
            app is designed to provide a seamless and efficient experience,
            making it easier for you to stay organized without any unnecessary
            hassle.
          </h2>
        </div>
        <div className="mt-7">
          <Link
            isExternal
            className={buttonStyles({
              color: "primary",
              radius: "full",
              variant: "shadow",
            })}
            href={siteConfig.links.docs}
          >
            Try it now
          </Link>
        </div>
      </div>
    </section>
  );
}
